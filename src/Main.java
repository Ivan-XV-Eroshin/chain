public class Main {

    public static void main(String[] args) {
        Notifier simple = new SimpleNotifier(Priority.COMMON);
        Notifier email = new EmailNotifier(Priority.IMPORTANT);
        Notifier sms = new SMSNotifier(Priority.ASAP);

        simple.setNextNotifier(email);
        email.setNextNotifier(sms);


    }
}
