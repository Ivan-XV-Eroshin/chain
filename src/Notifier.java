public abstract class Notifier {
    protected Priority priority;
    private Notifier nextNotifier;


    public Notifier(Priority priority) {
        this.priority = priority;
    }

    public void setNextNotifier(Notifier nextNotifier) {
        this.nextNotifier = nextNotifier;
    }

    void notifyManager (Priority outerPriority){
        writeClass(outerPriority);
        if (nextNotifier != null) {
            nextNotifier.notifyManager(outerPriority);
        }
    }

    abstract void writeClass(Priority priority);
}
