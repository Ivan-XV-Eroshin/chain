public class EmailNotifier extends  Notifier {
    public EmailNotifier(Priority priority) {
        super(priority);
    }

    @Override
    void writeClass(Priority outerPriority) {
        if (outerPriority == priority)
            System.out.println(this.getClass());
    }
}
