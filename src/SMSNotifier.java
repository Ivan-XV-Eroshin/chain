public class SMSNotifier extends  Notifier {
    public SMSNotifier(Priority priority) {
        super(priority);
    }

    @Override
    void writeClass(Priority outerPriority) {
        if (outerPriority == priority)
            System.out.println(this.getClass());
    }
}
