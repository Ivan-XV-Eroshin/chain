public class SimpleNotifier extends Notifier {
    public SimpleNotifier(Priority priority) {
        super(priority);
    }

    @Override
    void writeClass(Priority outerPriority) {
        if (outerPriority == priority)
            System.out.println(this.getClass());
    }
}
